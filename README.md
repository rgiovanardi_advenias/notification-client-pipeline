# Notification Client

Provides the UI components to list notifications.

WARNING: Before committing please run `yarn` so the `dist` package gets updated!

# Publish

  1. Bump the `package.json` version.
  2. Update CHANGELOG.md with changes.
  3. Publish to NPM registry:
    1. Login to NPM: `npm login`.
    2. Publish to NPM: `npm publish`

## Install

Run the following, also whenever you want to update to latest version as we currently don't go with npm registry.

```
yarn add epersonam-notification-client
```

## Work locally

If you want to avoid deploying this repo everytime you need to test a change you can use the local version of the `npm` package.

### Old method

1) Each time you change this project you have to hit `yarn`
2) on the client run something like `yarn add file:../epersonam-notification-client`

### New method

(see: https://classic.yarnpkg.com/en/docs/cli/link/)

1) Run from this project `yarn link`
2) Go to project where notification-client is used (i.e.: epersonam) and run `yarn link "epersonam-notification-client"`: this action will cause that project too use the local copy of the package, instead of the one versioned and published from remote repo.
3) When finished, just run `yarn unlink "epersonam-notification-client`.