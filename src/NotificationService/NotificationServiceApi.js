import axios from 'axios'
import configuration from '../Utils/configuration'

const api = () => {
  const api = axios.create()
  api.defaults.baseURL = window.config.notificationService.notifApiHost
  api.defaults.headers.common['Authorization'] = `Bearer ${configuration.getJwt()}`
  return api
}

export const Notification = {
  openNotification: (notification) => {
    if (!notification.id) {
      return Promise.resolve()
    }
    return api().patch(`/api/notifications/${notification.id}/opened`)
  },

  deleteNotification: (notification) => {
    if (!notification.id) {
      return Promise.resolve()
    }
    return api().delete(`/api/notifications/${notification.id}`)
  },
  ackNotified: (notifications) => {
    if (notifications.length == 0) {
      return Promise.resolve()
    }
    const notification_ids = notifications.map((e) => e.id)

    return api().post(`/api/notifications/notified`, {
      notification_ids: notification_ids
    })
  },
}

export const Question = {
  answerQuestion: (question) => {
    return api().post(`/api/questions`, {question})
  },
}
