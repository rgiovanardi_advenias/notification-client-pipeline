export class DeviceService {
  static DESKTOP_DEVICE = 1
  static TABLET_DEVICE = 2
  static MOBILE_DEVICE = 3

  static MAX_WITH_FOR_TABLET = 1023;
  static MAX_WITH_FOR_MOBILE = 500;

  static getCurrentDevice() {
    const width = this.getWidth();
    if (width <= this.MAX_WITH_FOR_MOBILE) {
      return this.MOBILE_DEVICE;
    } else if (width <= this.MAX_WITH_FOR_TABLET) {
        return this.TABLET_DEVICE;
    } else {
       return this.DESKTOP_DEVICE;
    }
  }

  static isMobile() {
    return this.getCurrentDevice() == this.MOBILE_DEVICE
  }

  static isTablet() {
    return this.getCurrentDevice() == this.TABLET_DEVICE
  }

  static isDesktop() {
    return this.getCurrentDevice() == this.DESKTOP_DEVICE
  }

  // https://stackoverflow.com/questions/1038727/how-to-get-browser-width-using-javascript-code
  static getWidth() {
    return Math.max(
      document.body.scrollWidth,
      document.documentElement.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.offsetWidth,
      document.documentElement.clientWidth
    );
  }
  
  static getHeight() {
    return Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.documentElement.clientHeight
    );
  }
}