import configuration from './Utils/configuration'
import { updateEventBus } from "./Utils/eventBus"
import { webSocketService } from './Utils/webSocketService'
import MenuContainer from './components/MenuContainer'

export default {
  install(Vue) {
    Vue.component('menuContainer', MenuContainer)

    if (Vue.prototype.$eventBus) {
      updateEventBus(Vue.prototype.$eventBus)
    }
  },
}

export {
  configuration,
  MenuContainer,
  webSocketService,
}
