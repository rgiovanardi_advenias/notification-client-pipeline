import { Socket } from 'phoenix'
import { eventBus } from './eventBus'
import configuration from './configuration'

export const webSocketService = {
  instance () {
    return this.socket || this.buildInstance()
  },

  buildInstance () {
    this.socket = new Socket(window.config.notificationService.notifSocketHost, {
      params: {
        token: configuration.getJwt(),
        user_infos: window.config.user,
      },
    })
    this.socket.onError((e) => eventBus.$emit('webSocketService.error', e))
    this.socket.onClose((e) => eventBus.$emit('webSocketService.close', e))
    this.socket.onOpen((e) => eventBus.$emit('webSocketService.open', e))
    this.socket.connect()
    return this.socket
  },

}
